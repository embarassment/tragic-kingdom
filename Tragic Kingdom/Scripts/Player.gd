extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

export var MOTION_SPEED = 140
const IDLE_SPEED = 10
var PlayerAnimNode
var RayNode 
var anim = ""
var animNew = ""

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_fixed_process(true)
	RayNode = get_node("RayCast2D")
	PlayerAnimNode = get_node("AnimatedSprite")
	
func _fixed_process(delta):
	var motion = Vector2()
	#motion
	
	if (Input.is_action_pressed("ui_up")):
		motion += Vector2(0, -1)
		RayNode.set_rotd(180)
	
	if (Input.is_action_pressed("ui_down")):
		motion += Vector2(0, 1)
		RayNode.set_rotd(0)
		
	if (Input.is_action_pressed("ui_left")):
		motion += Vector2(-1, 0)
		RayNode.set_rotd(-90)
		
	if (Input.is_action_pressed("ui_right")):
		motion += Vector2(1, 0)
		RayNode.set_rotd(90)
		
	motion = motion.normalized()*MOTION_SPEED*delta
	move(motion)
	
	#Animations
	
	if (motion.length() > IDLE_SPEED*0.09):
		if (Input.is_action_pressed("ui_up")):
			anim = "Walk_Up"
			
		if (Input.is_action_pressed("ui_down")):
			anim = "Walk_Down"
			
		if (Input.is_action_pressed("ui_right")):
			anim = "Walk_Right"
			
		if (Input.is_action_pressed("ui_left")):
			anim = "Walk_Left"
			
	else:
		if(RayNode.get_rotd() == 180):
			anim = "Idle_Up"
			
		if(RayNode.get_rotd() == 0):
			anim = "Idle_Down"
			
		if(RayNode.get_rotd() == 90):
			anim = "Idle_Right"
			
		if(RayNode.get_rotd() == -90):
			anim = "Idle_Left"
			
	if anim != animNew:
		animNew = anim
		PlayerAnimNode.play(anim)
